const emailRegex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
export const validateEmail = value => emailRegex.test(value) ? 'valid' : 'invalid'
