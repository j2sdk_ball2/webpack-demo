import { log } from './logger'
import { validateEmail } from './validator'

const container = document.createElement('div')
const span = document.createElement('span')
const text = document.createElement('input')
const button = document.createElement('input')

span.innerHTML = 'Email: '
text.setAttribute('id', 'email')
button.setAttribute('type', 'button')
button.setAttribute('value', 'Print')
button.onclick = () => {
    const e = document.getElementById('email')
    const { id, value } = e
    const valid = validateEmail(value)

    log(id, value, valid)
}

container.appendChild(span)
container.appendChild(text)
container.appendChild(button)

document.body.appendChild(container)
