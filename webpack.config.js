const path = require('path')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { resolve } = path

console.log('__dirname: ', __dirname, resolve(__dirname, 'dist'))

module.exports = {
  entry: {
    app: './src/index',
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
  },
  module: {
    rules: [
      // need for webpack-dev-server
      {
        test: /\.js$/,
        use: ['babel-loader'],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new UglifyJSPlugin(),
    new HtmlWebpackPlugin({
      title: 'Webpack Demo',
    })
  ],
  output: {
    filename: '[name].bundle.js',
    path: resolve(__dirname, 'dist'),
  },
}